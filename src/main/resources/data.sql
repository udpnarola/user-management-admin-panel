insert ignore into admin_panel_demo.authority values('ROLE_USER','USER');
insert ignore into admin_panel_demo.authority values('ROLE_ADMIN','ADMINISTRATOR');

insert ignore into admin_panel_demo.user
(id,email,first_name,last_name,password,user_name)
values(1,'admin@localhost','admin','admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','admin');

insert into admin_panel_demo.user_authorities (user_id,user_role)
SELECT * FROM (SELECT 1, 'ROLE_USER') AS tmp
WHERE NOT EXISTS (SELECT user_id, user_role FROM admin_panel_demo.user_authorities WHERE user_id = 1 and user_role='ROLE_USER') LIMIT 2;

insert into admin_panel_demo.user_authorities (user_id,user_role)
SELECT * FROM (SELECT 1, 'ROLE_ADMIN') AS tmp
WHERE NOT EXISTS (SELECT user_id, user_role FROM admin_panel_demo.user_authorities WHERE user_id = 1 and user_role='ROLE_ADMIN') LIMIT 2;

