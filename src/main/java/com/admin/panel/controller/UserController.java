package com.admin.panel.controller;

import com.admin.panel.service.UserService;
import com.admin.panel.service.dto.UserDto;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author puday
 */
@RestController
@RequestMapping("/api")
public class UserController {

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * @param userDto to save user
   * @return saved userDto
   */
  @PostMapping("/user")
  public UserDto saveUser(@Valid @RequestBody UserDto userDto) {
    return userService.saveUser(userDto);
  }


  /**
   * @param userDto to update user
   * @return updated userDto
   */
  @PutMapping("/user")
  public UserDto updateUser(@Valid @RequestBody UserDto userDto) {
    return userService.updateUser(userDto);
  }

  /**
   * @param userId to delete user
   * @return ok if user successfully deleted
   */
  @DeleteMapping("/user/{userId}")
  public ResponseEntity deleteUser(@PathVariable Long userId) {
    userService.deleteUser(userId);
    return ResponseEntity.ok().build();
  }

  /**
   * @param userId to get user by id
   * @return userDto if found
   */
  @GetMapping("/user/id/{userId}")
  public UserDto getUserById(@PathVariable Long userId){
    return userService.getUserById(userId);
  }

  /**
   * @param pageNumber to get page
   * @return page with users
   */
  @GetMapping("/user/{pageNumber}")
  public List<UserDto> getAllUser(@PathVariable Integer pageNumber) {
    return userService.getAllUsers(pageNumber);
  }

  /**
   * @param searchInput an input to find user
   * @return lit of matched users
   */
  @GetMapping("/user/search")
  public List<UserDto> searchUser(@RequestParam String searchInput){
    return userService.searchUser(searchInput);
  }

}
