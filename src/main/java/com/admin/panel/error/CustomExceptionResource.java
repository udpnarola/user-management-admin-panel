package com.admin.panel.error;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class CustomExceptionResource {

    @ExceptionHandler(CustomException.class)
    public String customException(CustomException e) {
        return e.getMessage();
    }
}
