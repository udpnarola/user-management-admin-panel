package com.admin.panel.service;

import com.admin.panel.model.User;
import com.admin.panel.service.dto.UserDto;
import java.util.List;

public interface UserService {

  UserDto saveUser(UserDto userDto);

  UserDto updateUser(UserDto userDto);

  void deleteUser(Long userId);

  UserDto getUserById(Long userId);

  List<UserDto> getAllUsers(Integer pageNumber);

  List<UserDto> searchUser(String searchInput);

}
