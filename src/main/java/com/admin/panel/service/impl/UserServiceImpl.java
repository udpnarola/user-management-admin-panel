package com.admin.panel.service.impl;

import com.admin.panel.config.WebSecurityConfig;
import com.admin.panel.dao.AuthorityDao;
import com.admin.panel.dao.UserDao;
import com.admin.panel.error.CustomException;
import com.admin.panel.model.User;
import com.admin.panel.service.UserService;
import com.admin.panel.service.dto.UserDto;
import com.admin.panel.service.mapper.AuthorityMapper;
import com.admin.panel.service.mapper.UserMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author puday
 */
@Service
public class UserServiceImpl implements UserService {

  private final UserDao userDao;
  private final AuthorityDao authorityDao;
  private final UserMapper userMapper;
  private final AuthorityMapper authorityMapper;
  private final WebSecurityConfig webSecurityConfig;

  public UserServiceImpl(UserDao userDao, AuthorityDao authorityDao, UserMapper userMapper, AuthorityMapper authorityMapper, WebSecurityConfig webSecurityConfig) {
    this.userDao = userDao;
    this.authorityDao = authorityDao;
    this.userMapper = userMapper;
    this.authorityMapper = authorityMapper;
    this.webSecurityConfig = webSecurityConfig;
  }

  @Override
  @Transactional
  public UserDto saveUser(UserDto userDto) {
    validateNewUser(userDto);
    User user = userMapper.toEntity(userDto);
    user.setPassword(webSecurityConfig.passwordEncoder().encode(userDto.getPassword()));
    userDto.getAuthorities().forEach(authorityDto -> {
      authorityDao.findById(authorityDto.getName()).ifPresent(user::addAuthority);
    });
    return userMapper.toDto(userDao.save(user));
  }

  private void validateNewUser(UserDto userDto) {
    if (userDto.getId() != null) {
      throw new CustomException("New User should not have an id");
    }
    if (userDto.getPassword() == null || userDto.getPassword().trim().isEmpty()) {
      throw new CustomException("The password can not be null or empty");
    }
    if (userDto.getConfirmPassword() == null || userDto.getConfirmPassword().trim().isEmpty()) {
      throw new CustomException("The confirm password can not be null or empty");
    }
    if (userDto.getPassword().compareTo(userDto.getConfirmPassword()) != 0) {
      throw new CustomException("The password is not matched, please confirm password again");
    }
    if (userDao.findByUserName(userDto.getUserName()).isPresent()) {
      throw new CustomException("User already exists with username " + userDto.getUserName());
    }
    if (userDao.findByEmail(userDto.getEmail()).isPresent()) {
      throw new CustomException("User already exists with email " + userDto.getEmail());
    }
  }

  @Override
  @Transactional
  public UserDto updateUser(UserDto userDto) {
    if (userDto.getId() == null) {
      throw new CustomException("User id can not be null");
    }
    User user = userDao.findById(userDto.getId()).orElseThrow(() -> new CustomException("User is not available for id " + userDto.getId()));
    user = userMapper.UpdateEntity(userDto, user);
    user.getAuthorities().removeAll(user.getAuthorities());
    User finalUser = user;
    userDto.getAuthorities().forEach(authorityDto -> {
      authorityDao.findById(authorityDto.getName()).ifPresent(finalUser::addAuthority);
    });
    return userMapper.toDto(finalUser);
  }

  @Override
  @Transactional
  public void deleteUser(Long userId) {
    User user = userDao.findById(userId).orElseThrow(() -> new CustomException("User is not available for id " + userId));
    user.getAuthorities().removeAll(user.getAuthorities());
    userDao.delete(user);
  }

  @Override
  @Transactional(readOnly = true)
  public UserDto getUserById(Long userId) {
    User user = userDao.findById(userId).orElseThrow(() -> new CustomException("User is not available for id " + userId));
    return userMapper.toDto(user);
  }

  @Override
  @Transactional(readOnly = true)
  public List<UserDto> getAllUsers(Integer pageNumber) {
    Pageable pageable = PageRequest.of(pageNumber, 5, Sort.by("userName"));
    return userDao.findAll(pageable).getContent()
        .stream()
        .map(userMapper::toDto)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public List<UserDto> searchUser(String searchInput) {
    return userDao.findByUserNameOrEmail(searchInput, searchInput)
        .stream()
        .map(userMapper::toDto)
        .collect(Collectors.toList());
  }

}
