package com.admin.panel.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author puday
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthorityDto {

  private String name;
  private String descr;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescr() {
    return descr;
  }

  public void setDescr(String descr) {
    this.descr = descr;
  }
}
