package com.admin.panel.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author puday
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

  private Long id;
  @NotNull(message = "first name can't be null")
  @NotEmpty(message = "first name can't be empty")
  private String firstName;
  @NotNull(message = "last name can't be null")
  @NotEmpty(message = "last name can't be empty")
  private String lastName;
  @NotNull(message = "Email can't be null")
  @NotEmpty(message = "Email can't be empty")
  @Email(message = "Email should be valid")
  private String email;
  @NotNull(message = "userName can't be null")
  @NotEmpty(message = "userName can't be empty")
  private String userName;
  private String password;
  private String confirmPassword;
  @NotEmpty(message = "Authorities can't be empty")
  private List<AuthorityDto> authorities = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public List<AuthorityDto> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(List<AuthorityDto> authorities) {
    this.authorities = authorities;
  }
}
