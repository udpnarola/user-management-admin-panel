package com.admin.panel.service.mapper;

import com.admin.panel.model.Authority;
import com.admin.panel.service.dto.AuthorityDto;
import org.springframework.stereotype.Component;

/**
 * @author puday
 */
@Component
public class AuthorityMapper {

  public AuthorityDto toDto(Authority authority){
    AuthorityDto authorityDto = new AuthorityDto();
    authorityDto.setName(authority.getName());
    authorityDto.setDescr(authority.getDescr());
    return authorityDto;
  }
}
