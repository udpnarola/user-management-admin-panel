package com.admin.panel.service.mapper;

import com.admin.panel.model.User;
import com.admin.panel.service.dto.UserDto;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * @author puday
 */
@Component
public class UserMapper {

  private final AuthorityMapper authorityMapper;

  public UserMapper(AuthorityMapper authorityMapper) {
    this.authorityMapper = authorityMapper;
  }

  public User toEntity(UserDto userDto) {
    User user = new User();
    user.setEmail(userDto.getEmail());
    user.setUserName(userDto.getUserName());
    user.setPassword(userDto.getPassword());
    return UpdateEntity(userDto, user);
  }

  public User UpdateEntity(UserDto userDto, User user) {
    if (userDto.getId() != null) {
      user.setId(userDto.getId());
    }
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    return user;
  }

  public UserDto toDto(User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setEmail(user.getEmail());
    userDto.setFirstName(user.getFirstName());
    userDto.setLastName(user.getLastName());
    userDto.setUserName(user.getUserName());
    userDto.setAuthorities(user.getAuthorities().stream().map(authorityMapper::toDto).collect(Collectors.toList()));
    return userDto;
  }


}
