package com.admin.panel.config;

import com.admin.panel.dao.UserDao;
import com.admin.panel.model.User;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Vishal Kotecha
 */
@Service
public class DomainUserDetailService implements UserDetailsService {

  @Autowired
  private UserDao userDao;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<User> user = userDao.findByUserName(username);
    if(!user.isPresent()) throw new UsernameNotFoundException("User not found!");
    return new DomainUserDetails(user.get());
  }
}
