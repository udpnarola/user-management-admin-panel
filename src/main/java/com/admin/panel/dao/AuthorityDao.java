package com.admin.panel.dao;

import com.admin.panel.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityDao extends JpaRepository<Authority, String> {

}
