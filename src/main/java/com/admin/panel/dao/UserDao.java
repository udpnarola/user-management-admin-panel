package com.admin.panel.dao;

import com.admin.panel.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Vishal Kotecha
 */
public interface UserDao extends JpaRepository<User,Long> {

  Optional<User> findByUserName(String userName);

  Optional<User> findByEmail(String email);

  List<User> findByUserNameOrEmail(String userName, String email);

}
